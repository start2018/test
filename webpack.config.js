const path = require('path')
const htmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    entry: path.join(__dirname, 'src', 'main.js'),
    // 要输出的文件
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    mode: 'development',
    devServer: {
        open: true,
        port: 8083,
        contentBase: 'src',
        hot: true
    },
    plugins: [
        // new webpack.HotModuleReplacementPlugin(),
        new htmlWebpackPlugin({
            // 指定模板页面
            template: path.join(__dirname, 'src', 'index.html'),
            // 生成的页面名称
            filename: 'index.html'
        })
    ],
    module: {
        rules: [{
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }, {
                test: /\.less$/,
                use: ['style-loader', 'css-loader', 'less-loader']
            }, {
                test: /\.scss$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            }, {
                test: /\.(jpg|png|jpeg|gif)$/,
                use: ['url-loader?limit=100000&name=img/[name].[hash:8].[ext]']
            }, {
                test: /\.(ttf|svg|eot|woff|woff2)$/,
                use: ['url-loader']
            }, {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/
            }, {
                test: /\.vue$/,
                use: 'vue-loader'
            }

        ]
    },
    // 更改vue的查找路径
    resolve: {
        alias: {
            // "vue$": "vue/dist/vue.js"
        }
    }


}