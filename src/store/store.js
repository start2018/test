import Vue from 'vue'
import Vuex from 'vuex'

// 使用响应式数据
Vue.use(Vuex)

// 一进来就读取本地存储数据赋值给car数组
var car = JSON.parse(localStorage.getItem('car') || '[]')
// 新建一个仓库
const store = new Vuex.Store({
    // 要设置的全局访问对象 相当于组件中的data
    state: {
        car: car,
        loadingFlag: false
    },
    mutations: {
        car(state, obj) {
            // 第一次进来的时候设置flag为false
            var flag = false
            state.car.some(item => {
                // 找到id值一样的就不追加，不一样的就追加到数组中
                if (obj.goodsId === item.goodsId) {
                    // 个数相加
                    item.goodsCount += parseInt(obj.goodsCount)
                    flag = true
                    return true
                }
            })
            if (!flag) {
                state.car.push(obj)
            }
            // 把这个购物车中的数据加到本地存储数据来达到持久化
            localStorage.setItem("car", JSON.stringify(state.car))
        },
        // 更新点击修改的个数,第二个参数为传过来的对象
        updataCount(state, goodsInfo) {
            // 找到id相等的那一个，修改
            state.car.some(item => {
                if (item.goodsId == goodsInfo.id) {
                    item.goodsCount = parseInt(goodsInfo.updataCount)
                    return true
                }
            })
            // 重新设置本地存储
            localStorage.setItem('car', JSON.stringify(state.car))
        },
        delGoods(state, id) {
            // 找到car中id相等的那一项删除
            state.car.some((item, index) => {
                if (item.goodsId == id) {
                    state.car.splice(index, 1)
                }
            })
            localStorage.setItem('car', JSON.stringify(state.car))
        },
        updateSelected(state, id, flag) {
            state.car.some(item => {
                if (item.id == id) {
                    item.goodsSelected = flag
                    return true
                }
            })
            localStorage.setItem('car', JSON.stringify(state.car))
        },
        // 定义Loading的显示状态,flag为main.js传过来的值
        loading_show(state, flag) {
            state.loadingFlag = flag
        }
    },
    // 此方法可以监听state的值
    getters: {
        getCarCount(state) {
            // 定义购物车的数值
            var c = 0
            // 便利出数组的项
            state.car.forEach(item => {
                c += item.goodsCount
            })
            return c
        },
        // 新建对象，方便根据id获取个数
        postCountAndId(state) {
            var obj = {}
            state.car.forEach(item => {
                obj[item.goodsId] = item.goodsCount
            })
            return obj
        },
        // 计算总价和总个数
        getPriceSum(state) {
            var obj = {
                count: 0,
                price: 0
            }
            state.car.forEach(item => {
                if (item.goodsSelected === true) {
                    obj.count++,
                        obj.price += item.goodsCount * item.goodsPrice
                }
            })
            return obj
        }
    }
})

// 导出
export default store;