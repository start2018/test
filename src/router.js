import VueRouter from 'vue-router'

import HomeContainer from './components/container/HomeContainer.vue'
import MemberContainer from './components/container/MemberContainer.vue'
import ShopcarContainer from './components/container/ShopcarContainer.vue'
import SearchContainer from './components/container/SearchContainer.vue'
import Newslist from './components/news/Newslist.vue'
import NewsInfo from './components/news/NewsInfo.vue'
import Photo from './components/pic/Photo.vue'
import PhotoInfo from './components/pic/PhotoInfo.vue'
import GoodsList from './components/goods/GoodList.vue'
import GoodsInfo from './components/goods/GoodInfo.vue'
import GoodsPhotoDetails from './components/goods/PhotoDetails.vue'
import GoodsComment from './components/goods/GoodsComment.vue'
import Feedback from './components/feedback/FeedBack.vue'
import Video from './components/video/Video.vue'

var router = new VueRouter({
    routes: [{
        path: '/',
        redirect: '/home'
    }, {
        path: '/home',
        component: HomeContainer
    }, {
        path: '/member',
        component: MemberContainer
    }, {
        path: '/search',
        component: SearchContainer
    }, {
        path: '/shopcar',
        component: ShopcarContainer
    }, {
        path: '/home/news',
        component: Newslist
    }, {
        path: '/home/newsinfo/:id',
        component: NewsInfo
    }, {
        path: '/home/photos',
        component: Photo
    }, {
        path: '/home/photo/photoInfo/:page',
        component: PhotoInfo,
        name: 'photoInfo'
    }, {
        path: '/home/goods',
        component: GoodsList
    }, {
        path: '/home/goods/goodsinfo',
        component: GoodsInfo,
        name: 'goodsinfo' //编程式导航
    }, {
        path: '/home/goods/goodsdetails/:id',
        component: GoodsPhotoDetails,
        name: 'goodsPicDetails'
    }, {
        path: '/goods/goodsComment',
        component: GoodsComment,
        name: 'goodsComment'
    }, {
        path: '/home/feedback',
        component: Feedback
    }, {
        path: '/home/video',
        component: Video
    }],
    linkActiveClass: 'mui-active',
    // 在路由跳转时让跳转页面显示在最顶端
    scrollBehavior(to, from, savedPosition) {
        return {
            x: 0,
            y: 0
        }
    }
})
// 导出
export default router