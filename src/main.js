import Vue from 'vue'
// 引入全局状态管理数据
import store from './store/store.js'
// 导入路由模块
import VueRouter from 'vue-router'
Vue.use(VueRouter)
// 引入vue-resource
import VueResource from 'vue-resource'
Vue.use(VueResource)
// 引入缩略图插件
import VuePreview from 'vue-preview'
Vue.use(VuePreview)

// 导入时间格式插件
import moment from 'moment'
// 引入本地加载json数据
// import axios from 'axios'
// 定义全局过滤器
Vue.filter('dateFormat', function(dataStr, pattern = "YYYY-MM HH:mm:ss") {
    return moment(dataStr).format(pattern)
})
// 设置请求的跟路径
Vue.http.options.root = 'https://www.apiopen.top'
Vue.http.options.emulateJSON = true;

// 引入mui的样式
import './lib/mui/css/mui.min.css'
import './lib/mui/css/icons-extra.css'

// 导入对应的组件 要显示懒加载图标就要全部导入mintui
import MintUi from 'mint-ui'
Vue.use(MintUi, {
    lazyload: {
        error: './static/404.png',
        loading: './static/photolazy.gif'
    }
})
import 'mint-ui/lib/style.css'
// 按需导入Mint-ui
/*import {
    Header,
    Swipe,
    SwipeItem,
    Toast,
    Button,
    Switch
} from 'mint-ui'*/
import router from './router.js'
import App from './App.vue'
// loading效果需要额外导入
/*import {
    Indicator
} from 'mint-ui'*/

// 使用懒加载技术
// Vue.use(Lazyload)
/*Vue.component(Header.name, Header)
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);
Vue.component(Button.name, Button);
Vue.component(Switch.name, Switch);*/


// 使用全http拦截器 响应前和响应后显示加载中，也可以自定义图片
Vue.http.interceptors.push((request, next) => {
    // mintui方法
    /*Indicator.open({
        // 底部的文字
        text: '加载中...',
        // 加载图标的类型
        spinnerType: 'fading-circle'
    });*/
    store.commit('loading_show', true)
    next((response) => {
        // Indicator.close()
        store.commit('loading_show', false)
    })
})

var vm = new Vue({
    el: '#app',
    render: c => c(App),
    router,
    // 使用全局仓库
    store,
})