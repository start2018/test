####介绍
一款仿手机APP构建的单页面应用，使用了vue vuex vue-router vue-resource mint-ui mui 等实现了部分功能：分类展示，主页面中的新闻列表，图片分享，详情，商品购买，结算，评论，回复

#### 安装教程
npm install
如遇到个别不能安装的依赖模块请单个安装

npm run dev

